/*******************************************************************************
*	File name:		hw1_do.do
*  	Author(s):		Royce Yang
*  	Written:       	1/26/2019
*
*	Inputs:			student_data.csv
*	
*	Outputs:		blueprint_data_cleaned.dta
*	
*	Description: 	Cleans the fake raw student data to answer questions for
*					Pset 1 of Levitt Data Class
*
*	1. The description should say what the goal of the Do-file is. 
*
*	2. Make your code legible, not just functional. 
*		a) Indent your loops, conditionals, preserve/restores, other code chunks
*		b) Line up your punctuation: braces ("{" and "}"), equal signs ("="), 
*		colons (":"), quote marks (`" "'), etc.
*		c) Use "///" to break up lines of code that extend beyond the line break
*		d) Make clear section headers
*		e) Comment on everything that is not inherently clear to a first-time 
*		reader! Comments should explain why not how. 
*
* 
*******************************************************************************/

 
**********************
// Preliminaries
**********************

version 15.1 	// tells future Stata versions that this .do file is written in 15.1
clear all 		// clears errything 
set more off 	// tells stata not to pause when "more" message displayed in console


**********************
// Import Data
**********************

cd "H:\downloads\" // your file path will look different
import delimited using student_data.csv, clear
// cd "$repository/"


********************************************************************************
*********                      Question 0: Naive Reg                  **********
********************************************************************************

* Your cleaning and regression code here
egen gender = group(sex), label
drop sex
rename gender sex
egen treated_n = group(treated), label
drop treated
rename treated_n treated
egen two_parent_n = group(two_parent), label
drop two_parent
rename two_parent_n two_parent
egen free_lunch_n = group(free_lunch), label
drop free_lunch
rename free_lunch_n free_lunch
egen school_n = group(school), label
drop school
rename school_n school
egen race_n = group(race), label
drop race
rename race_n race
destring exam_score_normalized, replace force
destring lagged_exam_score_normalized, replace force

// need to create dummies for categorical variables
// not sure if this is naieve but I will put it here because the instructions say to "create
// categorical variables" and I interpret the instructions as the need to create
// dummies
// tabulate school, gen(school)
// tabulate race, gen(race)
// just kidding, I think we can use factor variable (i.) instead

// I know this is naieve regression, but we cannot use i. on negative values, so we must clean classroom in order to run
// classroom is categorical because there is no meaning in its numerical values' order
// we make -99 to 0; I know I should make it . and then create a dummy, but this is naieve regression, so I will not do that here
replace classroom = 0 if classroom == -99

gen score_diff = (lagged_exam_score_normalized - exam_score_normalized)

regress score_diff treated // reg with no controls
regress score_diff treated age i.classroom sex two_parent free_lunch i.school i.race
						  

**********************
// Re-import
**********************
//cd "H:\downloads\" // your file path will look different
import delimited using student_data.csv, clear
//cd "$repository/"	  

						  
********************************************************************************
*********                      Question 1:                            **********
********************************************************************************

* Your cleaning and regression code here
replace sex = trim(sex) // remove possible extra whitespace
egen gender = group(sex), label
drop sex
rename gender sex

replace treated = trim(treated)
egen treated_n = group(treated), label
drop treated
rename treated_n treated

replace two_parent = trim(two_parent)
egen two_parent_n = group(two_parent), label
drop two_parent
rename two_parent_n two_parent

replace free_lunch = trim(free_lunch)
egen free_lunch_n = group(free_lunch), label
drop free_lunch
rename free_lunch_n free_lunch

replace school = "Washington" if school == "Washington." // typo? extra period after some

replace school = trim(school)
replace school = proper(school) // make upper/lowercase in names consistent
egen school_n = group(school), label
drop school
rename school_n school

replace race = trim(race)
egen race_n = group(race), label
drop race
rename race_n race

destring exam_score_normalized, replace force // force because NA is present and is a string
destring lagged_exam_score_normalized, replace force

replace two_parent = . if two_parent == 1 // 1 is actually missing
replace free_lunch = . if free_lunch == 1
replace race = . if race == 3

replace classroom = . if classroom == -99
gen miss_classroom = 1 if missing(classroom) // dummy for missing
replace classroom = 0 if miss_classroom == 1
replace miss_classroom = 0 if !(miss_classroom == 1) // 0 if not missing

drop if exam_score_normalized > 90 & !missing(exam_score_normalized) // drop outliers, random number 90 I chose that is less than the outliers I saw
drop if lagged_exam_score_normalized > 90 & !missing(lagged_exam_score_normalized) // drop outliers, random number 90 I chose that is less than the outliers I saw

gen score_diff = (lagged_exam_score_normalized - exam_score_normalized)

//tabulate school, gen(school) // use multiple dummies to represent the categorical variables
//tabulate race, gen(race)


********************************************************************************
*********                      Question 2:                            **********
********************************************************************************

regress score_diff treated // reg with no controls
regress score_diff treated age i.classroom sex two_parent free_lunch i.school i.race miss_classroom


********************************************************************************
*********                      Question 3:                            **********
********************************************************************************

hotelling exam_score_normalized age classroom sex two_parent free_lunch school race, by(treated)


********************************************************************************
*********                      Question 4:                            **********
********************************************************************************

hotelling exam_score_normalized age classroom sex two_parent free_lunch school race, by(treated)
hotelling lagged_exam_score_normalized age classroom sex two_parent free_lunch school race, by(treated)
mean(exam_score_normalized)
