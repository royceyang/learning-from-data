/*******************************************************************************
*
*   File name:		pset_3_random_forest.do
*  	Author(s):		Nagisa Tadjfar
*  	Written:       	2/26/2018
*
*	Inputs:			Airbnb Dataset
*
*	Outputs:		Predicted "review_scores_rating" variables
*
*	Description: 	Useful commands for cleaning data, running random forest
*
*******************************************************************************/
**********************
// Very helpful resource:
// https://www.stata.com/meeting/canada18/slides/canada18_Zou.pdf
**********************

**********************
// Loading Data
**********************

version 15.0 	// tells future Stata versions that this .do file is written in 15.1
clear all 		// clears errything
set more off 	// tells stata not to pause when "more" message displayed in console
cap log close 	// closes any previous log files

import delimited "/Users/royceyang/Desktop/learning-from-data/hw3/ps3data.csv", clear
merge 1:1 id using "https://gitlab.com/royceyang/learning-from-data/raw/master/hw3/sentimentAnalysis.dta"
// sentiment analysis of host_about variable

//keep id review_scores_rating review_scores_rating weekly_price sex age square_feet bed_type room_type indicator // keep variables you think would impact the outcome variable
rename indicator data_class // rename variables if helpful (1 = training data, 2 = testing data, 3 = testing data with higher prices)


/* At this point you should check your data to see if everything makes sense and clean if necessary */


egen bed_type_cat = group(bed_type) // create categorical variables if necessary
drop bed_type
rename bed_type_cat bed_type

egen room_type_cat = group(room_type)
drop room_type
rename room_type_cat room_type

egen sex_c = group(sex)
drop sex
rename sex_c sex

egen race_c = group(race)
drop race
rename race_c race

egen state_c = group(state)
drop state
rename state_c state

egen age_c = group(age)
drop age
rename age_c age

egen host_location_c = group(host_location)
drop host_location
rename host_location_c host_location

egen host_response_time_c = group(host_response_time)
drop host_response_time
rename host_response_time_c host_response_time

egen property_type_c = group(property_type)
drop property_type
rename property_type_c property_type

egen calendar_last_scraped_c = group(calendar_last_scraped)
drop calendar_last_scraped
rename calendar_last_scraped_c calendar_last_scraped

egen cancellation_policy_c = group(cancellation_policy)
drop cancellation_policy
rename cancellation_policy_c cancellation_policy

egen group_host_response_time_c = group(group_host_response_time)
drop group_host_response_time
rename group_host_response_time_c group_host_response_time

egen host_has_profile_pic_c = group(host_has_profile_pic)
drop host_has_profile_pic
rename host_has_profile_pic_c host_has_profile_pic

egen is_location_exact_c = group(is_location_exact)
drop is_location_exact
rename is_location_exact_c is_location_exact

egen group_cancellation_policy_c = group(group_cancellation_policy)
drop group_cancellation_policy
rename group_cancellation_policy_c group_cancellation_policy

gen sqft = real(square_feet)
drop square_feet
rename sqft square_feet

gen host_total_listings_count_c = real(host_total_listings_count)
drop host_total_listings_count
rename host_total_listings_count_c host_total_listings_count

gen week_day_ratio = weekly_price / price

gen desclen = (strlen(summary) + strlen(space) + strlen(description) + strlen(neighborhood_overview))
gen lnprice = log(price)
gen namelen = strlen(host_name)
gen aboutlen = log(strlen(host_about))
gen interaction_rate = host_response_rate * host_acceptance_rate
gen amenitieslen = strlen(amenities) - strlen(subinstr(amenities, ",", "", .))
gen accesslen = strlen(access)
gen interaction_len = strlen(interaction)
gen rules_len = (strlen(house_rules) - strlen(subinstr(house_rules, "No", "", .)))/2 + (strlen(house_rules) - strlen(subinstr(house_rules, "no", "", .)))/2

gen has_notes = 0
replace has_notes = 1 if strlen(notes) > 1

gen many_amenities = 0
replace many_amenities = 1 if amenitieslen > 4

gen many_rules = 0
replace many_rules = 1 if rules_len > 5

gen ln_sent_score = ln(host_about_score)

gen temp_str = substr(picture_url, -2, .)
gen image_high_quality = 1
replace image_high_quality = 0 if temp_str == "70"

drop if missing(review_scores_rating) & data_class == 1 // cannot have missing dependent variable in training data
sort data_class id

**********************
// 1.1
**********************



//regress review_scores_rating host_about_score ln_sent_score

//regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year rules_len amenitieslen ln_sent_score image_high_quality cleaning_fee if data_class == 1

//crossfold regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year rules_len amenitieslen ln_sent_score image_high_quality cleaning_fee if data_class == 1

//crossfold regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year rules_len amenitieslen ln_sent_score image_high_quality cleaning_fee if data_class == 1

//crossfold regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year image_high_quality if data_class == 1

//crossfold regress review_scores_rating host_is_superhost property_type accommodates first_review_year amenitieslen maximum_nights group_host_response_time host_location bathrooms

//lasso2 review_scores_rating week_day_ratio group_host_response_time property_type host_location aboutlen namelen lnprice desclen square_feet rules_len interaction_len accesslen amenitieslen image_high_quality host_response_rate host_acceptance_rate host_listings_count accommodates bathrooms bedrooms beds price security_deposit cleaning_fee guests_included extra_people maximum_nights availability_365 number_of_reviews host_is_superhost first_review_year if data_class == 1

//lasso2 review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year if data_class == 1, ///
//         plotpath(lnlambda) ///
//         plotopt(legend(off)) ///
//         plotlabel

//cvlasso review_scores_rating host_response_rate host_acceptance_rate host_listings_count accommodates bathrooms bedrooms beds price security_deposit cleaning_fee guests_included extra_people maximum_nights availability_365 number_of_reviews host_is_superhost first_review_year if data_class == 1
//cvlasso review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year if data_class == 1
//cvlasso, lopt
//regress review_scores_rating host_response_rate host_acceptance_rate host_listings_count accommodates bathrooms bedrooms beds price security_deposit guests_included availability_365 number_of_reviews host_is_superhost first_review_year if data_class == 1

//rlasso review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year if data_class == 1, robust
//rlasso review_scores_rating week_day_ratio group_host_response_time property_type host_location aboutlen namelen lnprice desclen square_feet rules_len interaction_len accesslen amenitieslen image_high_quality host_response_rate host_acceptance_rate host_listings_count accommodates bathrooms bedrooms beds price security_deposit cleaning_fee guests_included extra_people maximum_nights availability_365 number_of_reviews host_is_superhost first_review_year if data_class == 1
//di e(selected0)

//regress review_scores_rating host_acceptance_rate host_listings_count host_total_listings_count accommodates bathrooms beds price availability_365 host_is_superhost if data_class == 1

//use `testing_data', clear // load in the tempfil of testing data we saved earlier
//predict review_scores_rating_predicted // generate predicted values, can call this new variable anything!
//order id review_scores_rating_predicted // rearranges columns in your dataset, makes "browsing" easier
//br // notice a new column associated with predicted scores
//preserve
//keep id review_scores_rating_predicted
//export delimited "/Users/royceyang/Desktop/learning-from-data/hw3/predictions_1_OLS.csv", replace // export csv file with predictions
//restore



**********************
// 1.2
**********************

count // tells you how many rows are in the dataset currently
local total_rows = r(N) // save local variables that you can call later in loops, other commands, etc.
display `total_rows' // prints the value associated with a local variable
count if data_class == 1
local training_rows = r(N) // returns the total number of rows associated with the most recent count command, so in this case the "count if data_class == 1" command
display `training_rows'
count if data_class == 1 | data_class == 3
local testing_rows = r(N)
display `testing_rows'

preserve // preserve and restore allow you to manipulate your dataset and generate new datasets, graphs, run commands while being able to not actually change your dataset
keep if data_class == 2
replace review_scores_rating = 1 // Create a dummy number here, can be any # (just not missing).
tempfile testing_data // tempfiles are useful for defining cleaned, subsetted, or otherwise altered versions of your main dataset and allows you to use them without loading in a new file
save `testing_data', replace // associate a local variable with your tempfile that can be called later
restore // restore basically undoes all changes to your dataset done below the "preserve" command


* To run the random forest, specifying your type as a regression
//set seed 1234 // this can be any number but asssures any random choices made with random forest algorithm are consistent each time you run code
//randomforest review_scores_rating weekly_price sex age square_feet bed_type room_type in 1/`training_rows' , type(reg) iter(10) depth(10) lsize(20)

//randomforest review_scores_rating week_day_ratio group_host_response_time property_type host_location aboutlen namelen lnprice desclen square_feet rules_len interaction_len accesslen amenitieslen image_high_quality host_response_rate host_acceptance_rate host_listings_count accommodates bathrooms bedrooms beds price security_deposit cleaning_fee guests_included extra_people maximum_nights availability_365 number_of_reviews host_is_superhost first_review_year ln_sent_score in 1/`training_rows' , type(reg) iter(25) depth(25) lsize(25)
//ereturn list

regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year rules_len amenitieslen ln_sent_score image_high_quality cleaning_fee if data_class == 1

//regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year rules_len amenitieslen ln_sent_score image_high_quality cleaning_fee if data_class == 1

//randomforest review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year in 1/`training_rows' , type(reg) iter(25) depth(25) lsize(25)
//ereturn list // Provides summary of random forest results

* To generate predicted values once you have trained your model on training data
use `testing_data', clear // load in the tempfil of testing data we saved earlier
predict review_scores_rating_predicted // generate predicted values, can call this new variable anything!
order id review_scores_rating_predicted // rearranges columns in your dataset, makes "browsing" easier
br // notice a new column associated with predicted scores
preserve
keep id review_scores_rating_predicted
export delimited "/Users/royceyang/Desktop/learning-from-data/hw3/predictions_1_randomforest5.csv", replace // export csv file with predictions
restore

**********************
// 2.1
**********************

// Use same model as 1.1, but on different test set

//preserve // preserve and restore allow you to manipulate your dataset and generate new datasets, graphs, run commands while being able to not actually change your dataset
//keep if data_class == 3
//replace review_scores_rating = 1 // Create a dummy number here, can be any # (just not missing).
//tempfile testing_data // tempfiles are useful for defining cleaned, subsetted, or otherwise altered versions of your main dataset and allows you to use them without loading in a new file
//save `testing_data', replace // associate a local variable with your tempfile that can be called later
//restore // restore basically undoes all changes to your dataset done below the "preserve" command

//regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year rules_len amenitieslen ln_sent_score image_high_quality cleaning_fee if data_class == 1

**********************
// 2.2
**********************

// Use same model as 1.2 (which is include all the variables you can), but
// predicting on different test set

// Also note it is not as deep / not as many leaves because we are more scared of overfitting here

//randomforest review_scores_rating week_day_ratio group_host_response_time property_type host_location aboutlen namelen lnprice desclen square_feet rules_len interaction_len accesslen amenitieslen image_high_quality host_response_rate host_acceptance_rate host_listings_count accommodates bathrooms bedrooms beds price security_deposit cleaning_fee guests_included extra_people maximum_nights availability_365 number_of_reviews host_is_superhost first_review_year ln_sent_score in 1/`training_rows' , type(reg) iter(10) depth(15) lsize(15)
