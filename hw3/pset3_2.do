/*******************************************************************************
*
*   File name:		pset_3.do
*  	Author(s):		Royce Yang
*  	Written:       	3/11/2019
*
*	Inputs:			Airbnb Dataset
*
*	Outputs:		Predicted "review_scores_rating" variables
*
*	Description: 	OLS regression and random forest on Airbnb Dataset
*
*******************************************************************************/

**********************
// Loading Data
**********************

version 15.0 	// tells future Stata versions that this .do file is written in 15.1
clear all 		// clears errything
set more off 	// tells stata not to pause when "more" message displayed in console
cap log close 	// closes any previous log files

import delimited "/Users/royceyang/Desktop/learning-from-data/hw3/ps3data.csv", clear
// keep id review_scores_rating review_scores_rating weekly_price sex age square_feet bed_type room_type indicator host_location host_response_time 
rename indicator data_class // rename variables if helpful (1 = training data, 2 = testing data, 3 = testing data with higher prices)

count if data_class == 2

**********************
// Keep only training data
**********************
drop if missing(review_scores_rating) & data_class == 1 // cannot have missing dependent variable in training data
sort data_class id


count // tells you how many rows are in the dataset currently
local total_rows = r(N) // save local variables that you can call later in loops, other commands, etc.
display `total_rows' // prints the value associated with a local variable
count if data_class == 1
local training_rows = r(N) // returns the total number of rows associated with the most recent count command, so in this case the "count if data_class == 1" command
display `training_rows'
count if data_class == 1 | data_class == 2
local testing_rows = r(N)
display `testing_rows'

egen bed_type_cat = group(bed_type) // create categorical variables if necessary
drop bed_type
rename bed_type_cat bed_type

egen room_type_cat = group(room_type)
drop room_type
rename room_type_cat room_type

egen sex_c = group(sex)
drop sex
rename sex_c sex

egen race_c = group(race)
drop race
rename race_c race

egen state_c = group(state)
drop state
rename state_c state

egen age_c = group(age)
drop age
rename age_c age

egen host_location_c = group(host_location)
drop host_location
rename host_location_c host_location

egen host_response_time_c = group(host_response_time)
drop host_response_time
rename host_response_time_c host_response_time

egen property_type_c = group(property_type)
drop property_type
rename property_type_c property_type

egen calendar_last_scraped_c = group(calendar_last_scraped)
drop calendar_last_scraped
rename calendar_last_scraped_c calendar_last_scraped

egen cancellation_policy_c = group(cancellation_policy)
drop cancellation_policy
rename cancellation_policy_c cancellation_policy

egen group_host_response_time_c = group(group_host_response_time)
drop group_host_response_time
rename group_host_response_time_c group_host_response_time

egen group_cancellation_policy_c = group(group_cancellation_policy)
drop group_cancellation_policy
rename group_cancellation_policy_c group_cancellation_policy

gen sqft = real(square_feet)
drop square_feet
rename sqft square_feet

gen week_day_ratio = weekly_price / price

gen desclen = (strlen(summary) + strlen(space) + strlen(description) + strlen(neighborhood_overview))
gen lnprice = log(price)
gen namelen = strlen(host_name)
gen aboutlen = log(strlen(host_about))
gen interaction_rate = host_response_rate * host_acceptance_rate
gen amenitieslen = strlen(amenities)
gen accesslen = strlen(access)
gen interaction_len = strlen(interaction)
gen rules_len = strlen(house_rules)

**********************
// FIRST OLS REGRESSION
**********************


// regress review_scores_rating weekly_price sex bed_type room_type square_feet host_response_rate host_acceptance_rate host_listings_count week_day_ratio number_of_reviews
// correlate review_scores_rating host_response_rate host_acceptance_rate host_listings_count host_total_listings_count accommodates bathrooms bedrooms beds price security_deposit cleaning_fee guests_included extra_people maximum_nights availability_365 number_of_reviews host_is_superhost first_review_year

regress review_scores_rating host_is_superhost property_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price extra_people first_review_year if data_class == 1
//regress review_scores_rating namelen desclen lnprice aboutlen interaction_rate amenitieslen accesslen interaction_len rules_len host_response_time host_response_rate host_acceptance_rate sex race age host_listings_count state property_type room_type accommodates bathrooms bedrooms beds bed_type square_feet weekly_price monthly_price security_deposit cleaning_fee guests_included extra_people minimum_nights availability_30 availability_365 number_of_reviews cancellation_policy calculated_host_listings_count reviews_per_month week_day_ratio group_host_response_time host_is_superhost host_identity_verified require_guest_profile_picture require_guest_phone_verification instant_bookable first_review_year


**********************
// FIRST RANDOM FOREST REGRESSION
**********************


randomforest review_scores_rating weekly_price sex age square_feet bed_type room_type in 1/`training_rows' , type(reg) iter(10) depth(10) lsize(20)
predict review_scores_rating in 39720/49308

