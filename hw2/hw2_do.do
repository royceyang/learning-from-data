/*******************************************************************************
*	File name:		hw2_do.do
*  	Author(s):		Royce Yang
*  	Written:       	2/18/2019
*
*******************************************************************************/

 
**********************
// Preliminaries
**********************

version 15.1 	// tells future Stata versions that this .do file is written in 15.1
clear all 		// clears errything 
set more off 	// tells stata not to pause when "more" message displayed in console


**********************
// Import Data
**********************

// Question 2

cd "H:\downloads\hw2" // your file path will look different
// import delimited using states_population.csv, clear
import delimited using marijuana_legalization_status.csv, clear
//import delimited using fatalities_data_output.csv
// cd "$repository/"

sum treatment_legalization_year if treatment_legalization_year > 0, d
replace treatment_legalization_year = . if treatment_legalization_year == 0
replace medical_legalization_year = . if medical_legalization_year == 0
replace treatment_effective_year = . if treatment_effective_year == 0

// I think it makes sense to combine the year and weights since time is continuous
gen med_leg_year_weighted = medical_legalization_year + (1 - medical_legalization_weights)
gen treat_leg_year_weighted = treatment_legalization_year + (1 - treatment_legalization_weights)
gen treat_eff_year_weighted = treatment_effective_year + (1 - treatment_effective_weights)

rename state_names state

egen isControl = group(control)

// one-time only: save data as dta for later use in merge function
save marijuana_legalization_status, replace

//graph twoway scatter treatment_legalization_year medical_legalization_year


// Fatalities data and population data: below we merge together
// note that first time you run, uncomment the save command to save csv as dta
// so that we may use the merge command with the new dta file

import delimited using fatalities_data_output.csv, clear
save fatalities_data_output, replace
import delimited using states_population.csv, clear
rename state_names state
merge 1:1 state year using fatalities_data_output.dta

gen fat_percent = fatalities / population

gen nfat_percent = -fat_percent
sort nfat_percent
gen order = _n
tabdisp order if order <= 10, c(fat_percent state year)

// Question 3

bysort year: egen avg_fat=mean(fat_percent)

// merge marijuana data
drop _merge
merge m:1 state using marijuana_legalization_status.dta

gen ml = 0 // medical legalization dummy
replace ml = 1 if year > medical_legalization_year
replace ml = medical_legalization_weights if year == medical_legalization_year

gen tl = 0 // treatment legalization dummy
replace tl = 1 if year > treatment_legalization_year
replace tl = treatment_legalization_weights if year == treatment_legalization_year

gen te = 0 // treatment effective dummy
replace te = 1 if year > treatment_effective_year
replace te = treatment_effective_weights if year == treatment_effective_year

egen state_n = group(state), label
drop state
rename state_n state

/*regress fat_percent ml tl te
estimates store m1, title(Control for Marijuana Legalizations)

regress fat_percent i.state
estimates store m2, title(Control for States)

regress fat_percent i.state ml tl te
estimates store m3, title(Control for States and Marijuana Legalizations)

estout m1 m2 m3*/

qui reg fat_percent ml tl te
eststo model1

qui reg fat_percent i.state
eststo model2

qui reg fat_percent i.state ml tl te
eststo model3

qui reg fat_percent i.state ml tl te avg_fat
eststo model4

qui reg fat_percent i.state ml tl te if isControl == 1 // 1 is not control
eststo model5

save combined_output, replace

//esttab model1 model2 model3 model4

#delimit ; 
esttab model1 model2 model3 model4 model5 using "output.tex",
	se ar2 replace
	keep(ml tl te avg_fat 2.state 3.state 4.state _cons)
	coeflabels(2.state "Arizona" 
			   3.state "Arkansas"
			   4.state "California"
			   ml "Legalized Medical"
			   tl "Legalized Recreational"
			   te "Recreational in Effect")
	title("Marijuana Legalizations vs Fatality Rates")
	mtitles("\shortstack{Control for\\Marijuana Legalizations}" "Control for States" "\shortstack{Control for States and\\Marijuana Legalizations}" "\shortstack{Control for States and\\Marijuana Legalizations\\and Average Fatality}" "\shortstack{Control for States and\\Marijuana Legalizations\\and Average Fatality,\\Omitting Control States}")
	booktabs
 compress 

// question 4
psmatch2 ml tl te avg_fat i.state, out(fat_percent) common
